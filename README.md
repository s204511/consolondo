# consolondo

## Project setup
```
npm install
```
## Install firebase
```
npm install firebase
```
## Install date-fns
```
npm install date-fns
```
### Install carousel 
```
npm install vue3-carousel
```
### Install stylus 
```
npm install stylus stylus-loader --save-dev
```
### Install advanced cropper 
```
npm install -S vue-advanced-cropper@next
```
### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Deploys app to firebase
```
firebase deploy
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Design based on
https://getmdl.io/components/index.html#buttons-section