// Responsible: Lukas Petersen, s204511
import { ref, watchEffect } from 'vue'
import { db } from '@/firebase/config'
import { collection, orderBy, onSnapshot, query } from 'firebase/firestore'

const getCollection = (col) => {
    const error = ref(null)
    const documents = ref(null)
    let colRef = query(collection(db, col), orderBy('createdAt'))

    const unsub = onSnapshot(colRef, snapshot => {
        let results = []
        snapshot.docs.forEach(doc => {
            doc.data().createdAt && results.push({ ...doc.data(), id: doc.id })
        })

        // update values
        documents.value = results
        error.value = null
    })

    watchEffect((onInvalidate) => {
        // unsubscribe to previous collection
        onInvalidate(() => unsub())
    })
    
    return { documents, error }
}

export default getCollection