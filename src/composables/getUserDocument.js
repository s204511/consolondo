// Responsible: Simon Alsbirk, s204487
import { db } from "@/firebase/config";
import { collection, getDocs } from "firebase/firestore";

//This function is for getting a users document from the database, given a user's uid
const getUserDocument = async (userUID) => {
  const userUIDLowerCase = userUID.toLowerCase();
  const colRef = collection(db, "users");
  let result = null;
  
  await getDocs(colRef).then((snapshot) => {
    result = snapshot.docs.find((doc) => {
      return doc.id.toLowerCase() === userUIDLowerCase;
    });
  });
  if (typeof result === "undefined") {
    result = null;
  }
  return result;
};

const useGetUserDocument = () => {
  return { getUserDocument };
};
export default useGetUserDocument;
