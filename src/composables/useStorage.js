// Responsible: Lukas Petersen, s204511
import { storage } from "@/firebase/config";
import { ref } from "@vue/reactivity";
import getUser from "./getUser";
import { ref as storageRef, uploadBytes, getDownloadURL } from "firebase/storage";
import { doc, setDoc } from "firebase/firestore";
import { db } from "@/firebase/config";

const { user } = getUser()

const useStorage = () => {
    const error = ref(null)
    const url = ref(null)
    const filePath = ref(null)
    
    const uploadUserImage = async (file, isProfilePicture) => {
        // set the template string (using backtics)
        filePath.value = `pictures/users/${user.value.uid}/${file.name}`
        const storRef = storageRef(storage, filePath.value)

        try {
            
            const filename = file.name;
            const fileExtension = filename.substring(
                filename.lastIndexOf(".") + 1,
                filename.length
            );
            if (fileExtension != "jpg" && fileExtension != "png" && fileExtension != "jpeg") {
                throw new Error("File type not supported");
            }

            // this is so firebase knows that the file is a png, jpg or jpeg
            const metadata = {
                contentType: 'image/' + fileExtension,
            };

            //uploads the file to the storage
            const res = await uploadBytes(storRef, file, metadata)
            //Gets the storage URL of the file
            url.value = (await getDownloadURL(res.ref)).toString()

            const userModelRef = doc(db, 'users', user.value.uid)

            //Uploads the new storage URL to the database
            if (isProfilePicture) {
                await setDoc(userModelRef, { profilePictureURL: url.value }, { merge: true })
            } else {
                await setDoc(userModelRef, { backgroundPictureURL: url.value }, { merge: true })
            }
            
        } catch(e) {
            console.log(e.message)
            error.value = e.message
        }
    }

    const getImage = async (path) => {
        filePath.value = path
        const storRef = storageRef(storage, path)
        url.value = (await getDownloadURL(storRef)).toString()
    }

    return { error, url, filePath, uploadUserImage, getImage }
}

export default useStorage