// Responsible: Simon Alsbirk, s204487
import { ref } from "vue"
import { auth } from '../firebase/config'
import { createUserWithEmailAndPassword, updateProfile } from "firebase/auth"
import  createUser  from "@/composables/createUsermodel"

// Initialize the two refs to keep track of the sign in process
const error = ref(null)
const isPending = ref(false)


const signup = async (email, password, displayName) => {
    isPending.value = true

    try {
        const res = await createUserWithEmailAndPassword(auth, email, password)
        if (!res) {
            throw new Error('Could not complete the signup.')
        }
        await updateProfile(res.user, {'displayName': displayName})
        isPending.value = false
        const { createUserDocument } = createUser()
        await createUserDocument()
        
    } catch(errorCode) {
        console.log(errorCode.message)
        error.value = "Something went wrong. Try again"
        isPending.value = false
    }
}

const useSignup = () => {
    return { error, signup, isPending }
}

export default useSignup