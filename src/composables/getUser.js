// Responsible: Lukas Petersen, s204511
import { ref } from 'vue'
import { auth } from '@/firebase/config'
import { onAuthStateChanged, getUid } from 'firebase/auth'
import { doc, getDoc } from 'firebase/firestore'
import { db } from '@/firebase/config'

const user = ref(auth.currentUser)

// auth changes
onAuthStateChanged(auth, (_user) => {
    console.log('User state changed. Current user is: ', _user)
    user.value = _user
})

const getUserModel = async () => {
    const docRef = doc(db, 'users', user.value.uid)
    const userSnap = await getDoc(docRef)
    return { userSnap }
}

const getUser = () => {
    return { user, getUserModel }
}

export default getUser