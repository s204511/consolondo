// Responsible: Lukas Petersen, s204511
import { ref, watchEffect } from 'vue'
import { db } from '@/firebase/config'
import { collection, orderBy, onSnapshot, query } from 'firebase/firestore'

const getNestedCollection = (col, doc, nestedCol) => {
    const error = ref(null)
    const documents = ref(null)

    let colRef = collection(db, col, doc, nestedCol)

    const unsub = onSnapshot(colRef, snapshot => {
        let results = []
        snapshot.docs.forEach(doc => {
            results.push({ ...doc.data(), id: doc.id })
        })

        // update values
        documents.value = results
        error.value = null
    })

    watchEffect((onInvalidate) => {
        // unsubscribe to previous collection
        onInvalidate(() => unsub())
    })
    
    return { documents, error }
}

export default getNestedCollection