// Responsible: Simon Alsbirk, s204487
import { ref } from 'vue'
import { auth } from '../firebase/config'
import { signInWithEmailAndPassword } from 'firebase/auth'

// Initialize the two refs to keep track of the sign in process
const error = ref(null)
const isPending = ref(false)

const login = async (email, password) => {
    isPending.value = true

    try {
        await signInWithEmailAndPassword(auth, email, password)
        isPending.value = false
    } catch(errorCode) {
        error.value = errorCode.code
        isPending.value = false
    }
}

const useLogin = () => {
    return { error, login, isPending }
}

export default useLogin