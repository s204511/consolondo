// Responsible: Simon Alsbirk, s204487
import { ref } from "vue";
import { auth } from "../firebase/config";
import {
  GoogleAuthProvider,
  FacebookAuthProvider,
  signInWithPopup,
} from "firebase/auth";

import  createUser  from "@/composables/createUsermodel"
import  getUser  from '@/composables/getUser'
import useGetUserDocument from "@/composables/getUserDocument";

// Initialize the two refs to keep track of the sign in process
const error = ref(null);
const isPending = ref(false);

const googleSignIn = async () => {
  isPending.value = true;
  const provider = new GoogleAuthProvider();

  //Makes sure we fetch the user's email, name and profile picture
  provider.addScope("profile");
  provider.addScope("email");

  try {
    await signInWithPopup(auth, provider)
    isPending.value = false
    postSignIn()

  //Catch the error if the user failed to sign in
  } catch (errorCode) {
    error.value = "Something went wrong. Try again" 
    isPending.value = false
  }
};


const postSignIn = async () => {
  const { user } = getUser();
  const { getUserDocument } = useGetUserDocument();

  //if there is no document on the user in the database create one. 

  const userDocument = await getUserDocument(user.value.uid);
  if (!userDocument) {
    const { createUserDocument } = createUser()
    await createUserDocument()
  }
}


const facebookSignIn = async () => {
  isPending.value = true;
  const facebookProvider = new FacebookAuthProvider();

  //Makes sure we fetch the user's email, name and profile picture
  facebookProvider.addScope("email");
  facebookProvider.addScope("public_profile");

  try {
    await signInWithPopup(auth, facebookProvider);
    isPending.value = false;
    postSignIn()

  //Catch the error if the user failed to sign in
  } catch (errorCode) {
    error.value = "Something went wrong. Try again";
    isPending.value = false;
  }
};

const useThirdPartySignIn = () => {
  return { error, googleSignIn, facebookSignIn, isPending };
};

export default useThirdPartySignIn;
