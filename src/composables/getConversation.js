// Responsible: Lukas Petersen, s204511
import { ref, watchEffect } from 'vue'
import { db } from '@/firebase/config'
import { collection, orderBy, onSnapshot, query } from 'firebase/firestore'

const error = ref(null)
const documents = ref(null)
const conId = ref(null)
const conIdList = ref(null)

const fetchConversation = (uid, cid) => {
    // if userID or conversation ID is not found, terminate
    if(!(uid && cid)) {
        return { }
    }

    let colRef = query(collection(db, 'conversations', cid, 'messages'), orderBy('createdAt'))
    const unsub = onSnapshot(colRef, snapshot => {
        let results = []
        snapshot.docs.forEach(doc => {
            doc.data().createdAt && results.push({ ...doc.data(), id: doc.id })
        })

        // update values
        documents.value = results
        error.value = null
    })

    watchEffect((onInvalidate) => {
        // unsubscribe to previous collection
        onInvalidate(() => unsub())
    })
    return { documents, error }
}

const getConversation = () => {
    return { error, documents, conId, conIdList, fetchConversation }
}

export default getConversation