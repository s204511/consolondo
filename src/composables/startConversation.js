// Responsible: Lukas Petersen, s204511
import { db } from "@/firebase/config"
import { useRouter } from "vue-router"
import getUser from "./getUser"
import { collection, getDoc, setDoc, doc, addDoc } from '@firebase/firestore'
import { timestamp } from '@/firebase/config'

const startConversation = () => {
    const { user, getUserModel } = getUser()
    const router = useRouter()

    const handleClick = async (id, name) => {
        try {
            // get the conPartner image
            const partnerRef = doc(db, 'users', id)
            const partnerSnap = await getDoc(partnerRef)
            // get the conStarter image
            const { userSnap } = await getUserModel()
    
            await setDoc(doc(db, 'conversations', `${user.value.uid}${id}`), {
                conPartnerId: id,
                conPartnerImage: partnerSnap.data().profilePictureURL,
                conPartnerName: name,
                conStarterId: user.value.uid,
                conStarterImage: userSnap.data().profilePictureURL,
                conStarterName: user.value.displayName,
                latestActivity: timestamp()
            }, { merge: true })
    
            // set the references to the new conversation for both the user models
            const conStarterRef = collection(db, 'users', user.value.uid, 'conversations')
            await addDoc(conStarterRef, { conId: `${user.value.uid}${id}` })
            const conPartnerRef = collection(db, 'users', id, 'conversations')
            await addDoc(conPartnerRef, { conId: `${user.value.uid}${id}` })
    
            // go to messages
            router.push({ name: "Messages" });
        } catch(e) {
            console.log(e.message)
        }
    }

    return { handleClick }
}

export default startConversation