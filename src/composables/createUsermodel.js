// Responsible: Simon Alsbirk, s204487
import {db} from '../firebase/config'
import { doc, setDoc }from 'firebase/firestore'
import  getUser  from '@/composables/getUser'

const createUserDocument = async () => {
    const  { user } = getUser();

    let profilePictureURLFromUser = null
    if (user.value.profilePictureURL) {
        profilePictureURLFromUser = user.value.profilePictureURL
    }

    if(!profilePictureURLFromUser) {
        // if no existing profile picture: set to the default picture
        profilePictureURLFromUser = 'https://firebasestorage.googleapis.com/v0/b/consolondo-b2ee2.appspot.com/o/pictures%2Fusers%2FprofilePicture.png?alt=media&token=1411550e-0918-4003-ad8e-7832066582c6'
    }
	await setDoc(doc(db, "users", user.value.uid), {
        displayName: user.value.displayName,
        email: user.value.email,
        specialties: [],
        description: 'No description.',
        shortdescription: 'No description.',
        profilePictureURL: profilePictureURLFromUser,
        // set the background picture to the default picture
        backgroundPictureURL: 'https://firebasestorage.googleapis.com/v0/b/consolondo-b2ee2.appspot.com/o/pictures%2Fusers%2FbackgroundPicture.png?alt=media&token=54dd84f4-f0d7-419b-bdd9-440b404e3321'
    })
}

const createUser = () => {
    return { createUserDocument }
}

export default createUser



