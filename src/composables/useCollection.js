// Responsible: Andreas Rasmussen, s204470
import { ref } from 'vue'
import { db } from '@/firebase/config'
import { addDoc, collection } from 'firebase/firestore'

const useCollection = (col) => {
    const error = ref(null)
    const isPending = ref(false)

    const addDocument = async (doc) => {
        error.value = null
        isPending.value = true

        try {
            const colRef = collection(db, col)
            await addDoc(colRef, doc)
            isPending.value = false
        } catch(e) {
            console.log(e.message)
            error.value = 'Could not send message.'
            isPending.value = false
        }
    }
    return { error, addDocument, isPending }
}

export default useCollection