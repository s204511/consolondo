<!-- Responsible: Simon Alsbirk, s204487 -->
<!-- This component is for uploading a picture, cropping a picture, and setting the new picture
as either a new profile picture or background picture -->
<template>
  <!-- This div contains the interaction of uploading an image -->
  <div v-if="!selectedPictureToUpload" class="uploadImageDiv">
    <h5 class="headerText">Upload File</h5>

    <!-- This div contains the drag and drop interactions.  -->
    <div
      :data-active="active"
      @dragenter.prevent="setActive"
      @dragover.prevent="setActive"
      @dragleave.prevent="setInactive"
      @drop.prevent="onDrop"
    >
      <!-- Label so you can press anywhere in the grey box to open the file explorer -->
      <label
        class="dragAndDropField"
        :class="{ stripedContainer: active }"
        for="file-input"
      >
        <!-- Although it isn't visible, this input controls the accepted file formats,
          and what happens after a file is uploaded-->
        <!-- for now just PNG later: accept="image/jpeg, image/png, image/jpg". This requires that 
		      all other uses of profilepicture doesn't search for profilePicture.png -->
        <input
          hidden
          type="file"
          id="file-input"
          accept="image/png, image/jpeg, image/jpg"
          @change="onInputChange"
        />

        <!-- These last tags inside label is just the instructions text and the cloud icon -->
        <span v-if="active" class="uploadInstructions"> Drop image here </span>
        <span v-else class="uploadInstructions">
          Drag and drop an image file here or click
        </span>
        <div class="spacer"></div>
        <i class="material-icons uploadIcon">cloud_upload</i>
      </label>
    </div>

    <div class="errorMessageAndCancelButtonDiv">
      <p class="uploadErrorMessage">{{ error }}</p>
      <button class="mdl-button mdl-js-button bottomButton" @click="closePage">
        CANCEL
      </button>
    </div>
  </div>

  <!-- This div contains the interaction of cropping an image -->
  <div v-else>
    <div v-if="!loading">
      <div class="cropperDiv">
        <h5 class="headerText">Crop Picture</h5>
        <!-- This div contains the advanced cropper component -->
        <div class="cropperHolder">
          <!-- If we want to crop a profilepicture the aspectratio must be square -->
          <div v-if="isProfilePicture">
            <cropper
              class="cropper"
              ref="cropper"
              :src="selectedPictureToUpload"
              :stencil-props="{
                aspectRatio: 1 / 1,
              }"
            />
          </div>
          <!-- whereas for a backgroundpicture the aspectratio must be 5:1 -->
          <div v-else>
            <cropper
              class="cropper"
              ref="cropper"
              :src="selectedPictureToUpload"
              :stencil-props="{
                aspectRatio: 5 / 1,
              }"
            />
          </div>
        </div>

        <!-- This div holds the upload and cancel buttons displayed on the bottom -->
        <div class="uploadAndCancelButtonDiv">
          <button
            @click="uploadNewImage"
            class="mdl-button mdl-js-button bottomButton"
          >
            SAVE IMAGE
          </button>
          <div class="spacer"></div>
          <button
            class="mdl-button mdl-js-button bottomButton"
            @click="closePage"
          >
            CANCEL
          </button>
        </div>
      </div>
    </div>

    <!-- When the user has pressed upload button and we wait for it to upload -->
    <div v-else>
      <div class="loadingDiv">
        <progress />
      </div>
    </div>
  </div>
</template>

<script>
import { ref } from "vue";
import { onMounted, onUnmounted } from "vue";
import useStorage from "@/composables/useStorage";

/* An externally imported library for cropping images */
import { Cropper } from "vue-advanced-cropper";
import "vue-advanced-cropper/dist/style.css";

export default {
  name: "UploadImageComponent",
  components: { Cropper},

  // Whether the user is uploading a profile picture or a background picture
  props: ["isProfilePicture"],

  emits: ["close"],
  setup(props, { emit }) {

    const { isProfilePicture } = props;
    const { uploadUserImage } = useStorage();

    /* constants and variables for uploading the picture */
    const error = ref("");
    const selectedPictureToUpload = ref(null);
    let active = ref(false);
    // to stop the drag and drop field from flickering.
    let inActiveTimeout = null;

    // event listeners for the drag and drop field
    const events = ["dragenter", "dragover", "dragleave", "drop"];


    /* constants for the cropping of the picture */
    const cropper = ref(null);

    //loading is true when the picture is being uploaded
    const loading = ref(false);
    
    // The canvas that holds the image that is being cropped
    let croppedCanvas = null;


    /* Functions for uploading the picture */

    // This function is called when the user has selected a png image file 
    // (file type checks has been done in parent methods)
    // that he wants to crop and upload.
    // function converts the file into a picture and sets it as the selectedPictureToUpload
    const addFile = async (newFile) => {
      const reader = new FileReader();

      reader.onload = function (e) {
        selectedPictureToUpload.value = reader.result;
      };

      reader.readAsDataURL(newFile);
      error.value = "";
    };

    // This function is called then the user has uploaded the image via file explorer
    const onInputChange = (e) => {
      addFile(e.target.files[0]);
    }

    // setActive and setInactive use timeouts, so that when you drag an item over a child element,
    // the dragleave event that is fired won't cause a flicker. A few ms should be plenty of
    // time to wait for the next dragenter event to clear the timeout and set it back to active.
    const setActive = () => {
      active.value = true;
      clearTimeout(inActiveTimeout);
    }
    const setInactive = () => {
      inActiveTimeout = setTimeout(() => {
        active.value = false;
      }, 50);
    }

    // This function is called when the user drops an image file onto the drag and drop field
    const onDrop = (e) => {
      const file = e.dataTransfer.files[0];
      const filename = file.name;

      // Only accept file types that are png
      const fileExtension = filename.substring(
        filename.lastIndexOf(".") + 1,
        filename.length
      );
      if (fileExtension == "png" || fileExtension == "jpg" || fileExtension == "jpeg") {
        addFile(e.dataTransfer.files[0]);
      } else {
        error.value = "Only PNG files are allowed";
      }

      setInactive();
    }

    // Adds listeners so default actions won't be triggered when the user drags an image over and onto the drop field
    const preventDefaults = (e) =>{
      e.preventDefault();
    }
    onMounted(() => {
      events.forEach((eventName) => {
        document.body.addEventListener(eventName, preventDefaults);
      });
    });
    onUnmounted(() => {
      events.forEach((eventName) => {
        document.body.removeEventListener(eventName, preventDefaults);
      });
    });

    // The only function specifically for cropping the picture.
    // this function is called when the user has cropped the image and wants to upload it.
    const uploadNewImage = async () => {
      const {canvas} = cropper.value.getResult()

      loading.value = true;
      //const dataURL = croppedCanvas.toDataURL();
      const dataURL = canvas.toDataURL();
      const blob = await (await fetch(dataURL)).blob();
      let file = null;
      if (isProfilePicture) {
        file = new File([blob], "profilePicture.png");
      } else {
        file = new File([blob], "backgroundPicture.png");
      }
      await uploadUserImage(file, isProfilePicture);

      loading.value = false;

      //Close the page and set true to indicate that a new picture has been uploaded
      emit("close", true);
    };

    // This function is called when the user presses the cancel button.
    const closePage = () => {
      // Close the page and set false to indicate no image has been uploaded
      emit("close", false);
    };

    return {
      active,
      addFile,
      onInputChange,
      setActive,
      setInactive,
      onDrop,
      selectedPictureToUpload,
      uploadNewImage,
      error,
      closePage,
      loading,
      cropper
    };
  },
};
</script>

<!-- Everything inside this style tag is css modifications to the cropper library -->
<style>
/* Makes the line and rectangles to indicate where user is cropping primary color */
.vue-simple-handler {
  background: var(--primary);
}
.vue-simple-line {
  border-color: var(--primary);
}

/* Defines the height and width of the cropper */
.cropper {
  height: 200px;
  width: 400px;
  padding: 40px;
}

/* fills the space where the image dimensions don't fit the cropperdimensions defined above
 with white colour */
.vue-advanced-cropper__background,
.vue-advanced-cropper__foreground {
  background-color: #ffffff;
}

/* default property that must be none for the cropper to look proper */
.vue-preview__image, .vue-advanced-cropper__image{
  max-height: none;
}
</style>

<style scoped>
/* The div that contains the loading animation */
.loadingDiv {
  width: 540px;
  height: 401px;
  background-color: white;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 5px;
}

/* The div that contains the entire cropper page */
.cropperDiv {
  display: flex;
  flex-direction: column;
  justify-content: center;
  background-color: white;
  border-radius: 5px;
  padding: 0px 30px;
}

.uploadAndCancelButtonDiv {
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  padding: 0 0 10px;
}

/* The div that only contains the cropper library component*/
.cropperHolder {
  display: flex;
  background-color: #ffffff;
  justify-content: center;
  align-items: center;
  padding: 0 0px 10px;
}

.uploadErrorMessage {
  margin: 5px 0 0;
  color: red;
}

/* Styles for all the textbuttons in the bottom of this component */
.bottomButton,
.bottomButton:hover,
.bottomButton:active {
  padding: 0;
  font-weight: 500;
  text-transform: none;
  color: #000;
  background-color: rgba(0, 0, 0, 0);
  color: var(--primary);
}

.errorMessageAndCancelButtonDiv {
  width: 404px;
  justify-content: space-between;
  display: flex;
  padding: 10px 0 5px 0;
}

/* Provides empty space between tags */
.spacer {
  width: 25px;
  height: 15px;
}

.uploadIcon {
  color: #706d6d;
  font-size: 30px;
}
/* The text displayed inside the drag and drop field */
.uploadInstructions {
  color: #706d6d;
  text-align: center;
  font-size: 16px;
}

.dragAndDropField {
  justify-content: center;
  display: flex;
  align-items: center;
  flex-direction: column;
  background: #e5e5e5;
  border-style: dashed;
  border-width: 2px;
  border-color: #b7b6b6;
  width: 400px;
  height: 150px;
}
/* Makes the drag and drop field striped when dragging a file above it */
.stripedContainer {
  background-color: none;
  /* normal border */
  border-style: solid;
  /* border thickness */
  border-width: 2px;
  border-color: #a8a8a8;
  border-radius: 0.2px;
  background: repeating-linear-gradient(
    135deg,
    #e5e5e5 0px,
    #e5e5e5 20px,
    #a8a8a8 20px,
    #a8a8a8 40px
  );
}

.headerText {
  color: #000;
  text-align: left;
  padding: 25px 0 20px 0;
  font-weight: 600;
  margin: 0;
}

.uploadImageDiv {
  background-color: #fff;
  border-radius: 5px;
  padding: 0px 30px;
}

.input,
textarea {
  border-bottom: none;
}

</style>