import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LoginView from '../views/auth_views/LoginView.vue'
import SignupView from '../views/auth_views/SignupView.vue'
import MessagesView from '../views/MessagesView.vue'
import ProfileView from '../views/ProfileView.vue'
import getUser from '../composables/getUser'
import SearchView from '../views/SearchView.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: HomeView
  },
  {
    path: '/login',
    name: 'Login',
    component: LoginView
  },
  {
    path: '/signup',
    name: 'Signup',
    component: SignupView
  },
  {
    path: '/messages',
    name: 'Messages',
    component: MessagesView
  },
  {
    path: '/profile/:userNameAndUserUID',
    name: 'Profile',
    component: ProfileView,
    props: true
  },
  {
    path: '/search/:searchTerm',
    name: 'Search',
    component: SearchView,
    props: true
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to,from,next) => {
  const { user } = getUser();
  //let stopWatch = null; 
  if (to.path === "/login" || to.path === "/signup") {
    if (user.value) {
      next({ name: "Home" })
      return;
    }
   /* stopWatch = watch(user, () => {
      if (user.value) {
        next({ name: "Home" })
      }
    })*/
  }
  next();
})


export default router
