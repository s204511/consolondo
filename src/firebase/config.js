import { initializeApp } from 'firebase/app'
import { getFirestore, serverTimestamp } from 'firebase/firestore'
import { getAuth } from 'firebase/auth'
import { getStorage } from 'firebase/storage'

const firebaseConfig = {
    apiKey: "AIzaSyCA7uy_53oBVJe0z3Q1UOuWB2n_LSiPX08",
    authDomain: "consolondo-b2ee2.firebaseapp.com",
    projectId: "consolondo-b2ee2",
    storageBucket: "consolondo-b2ee2.appspot.com",
    messagingSenderId: "440541026263",
    appId: "1:440541026263:web:6197f9daf3796f03d29d92",
    measurementId: "G-H2G9XNLMPG"
  };

// init firebase
const app = initializeApp(firebaseConfig)

// init firestore
const db = getFirestore()

// init authentication
const auth = getAuth(app)

// init storage
const storage = getStorage(app)

// timestamp
const timestamp = serverTimestamp

export { db, auth, storage, timestamp}