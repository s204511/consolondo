import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { auth } from '@/firebase/config'
import { onAuthStateChanged } from 'firebase/auth'

// Global styles
import './assets/main.css'

//wait for auth to be initialized
let app
onAuthStateChanged (auth, () => {
  if ( !app ) {
	  app = createApp(App).use(router).mount('#app')
  }
})

//createApp(App).use(router).mount('#app')
